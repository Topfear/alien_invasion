import pygame
from button import Button

from settings import Settings
from game_stats import GameStats
from scoreboard import Scoreboard
from ship import Ship
import game_functions as gf
from pygame.sprite import Group


def run_game():
    pygame.init()
    ai_settings = Settings()

    screen = pygame.display.set_mode(
        (ai_settings.screen_width, ai_settings.screen_height))
    pygame.display.set_caption("Alien Invasion")

    # создание кнопки Play
    play_button = Button(ai_settings, screen, "Play")

    # создание статистики
    stats = GameStats(ai_settings)
    sb = Scoreboard(ai_settings, screen, stats)
    # создание корабля
    ship = Ship(screen=screen, ai_settings=ai_settings)
    # создание группы пуль
    bullets = Group()
    # создание пришельцев
    aliens = Group()
    gf.create_fleet(ai_settings=ai_settings, screen=screen, aliens=aliens, ship=ship)

    # таймер
    clock = pygame.time.Clock()

    while True:
        clock.tick(ai_settings.fps)

        gf.check_events(
            ai_settings=ai_settings, screen=screen, 
            ship=ship, bullets=bullets, 
            stats=stats, play_button=play_button, 
            aliens=aliens, sb=sb)

        if stats.game_active:
            ship.update()
            gf.update_bullets(
                ai_settings=ai_settings, screen=screen, 
                ship=ship, bullets=bullets, aliens=aliens, 
                stats=stats, sb=sb)
            gf.update_aliens(
                ai_settings=ai_settings, aliens=aliens, 
                ship=ship, stats=stats, 
                screen=screen, bullets=bullets,
                sb=sb)

        gf.update_screen(
            ai_settings=ai_settings, screen=screen, 
            ship=ship, bullets=bullets, aliens=aliens, 
            stats=stats, play_button=play_button,
            sb=sb)

run_game()
