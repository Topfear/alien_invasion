

DEBUG = False


class Settings():

    def __init__(self):
        # настройки экрана
        self.screen_width = 1200
        self.screen_height = 800
        self.bg_color = (230, 230, 230)
        self.fps = 60

        # настройки корабля
        self.ship_limit = 3

        # настройки пули
        self.bullet_collision_delete = False if DEBUG else True
        self.bullet_width = 500 if DEBUG else 3
        self.bullet_height = 15
        self.bullet_color = 60, 60, 60
        self.bullets_allowed = 10 if DEBUG else 3

        # настройки пришельцев
        self.fleet_drop_speed = 10

        # ускорение игры
        self.speedup_scale = 1.1
        
        # темп роста стоимости пришельцев
        self.score_scale = 1.5

        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        self.ship_speed_factor = 10
        self.bullet_speed_factor = 5
        self.alien_speed_factor = 5

        self.fleet_direction = 1 # 1 вправо, -1 влево

        # очков за пришельца
        self.alien_points = 50

    def increase_speed(self):
        # увеличение скорости игры
        self.ship_speed_factor *= self.speedup_scale
        self.bullet_speed_factor *= self.speedup_scale
        self.alien_speed_factor *= self.speedup_scale

        # увеличение очков
        self.alien_points = int(self.alien_points * self.score_scale)
