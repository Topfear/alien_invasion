import sys
from time import sleep

import pygame

from bullet import Bullet
from alien import Alien
from settings import Settings


def check_keydown_events(event, ai_settings: Settings, screen, ship, bullets):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = True
    elif event.key == pygame.K_LEFT:
        ship.moving_left = True
    if event.key == pygame.K_UP:
        ship.moving_up = True
    elif event.key == pygame.K_DOWN:
        ship.moving_down = True
    elif event.key == pygame.K_SPACE:
        fire_bullet(ai_settings, screen, ship, bullets)
    elif event.key == pygame.K_q:
        sys.exit()


def check_keyup_events(event, ship):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = False
    elif event.key == pygame.K_LEFT:
        ship.moving_left = False
    if event.key == pygame.K_UP:
        ship.moving_up = False
    elif event.key == pygame.K_DOWN:
        ship.moving_down = False


def check_events(ship, ai_settings, bullets, screen, stats, play_button, aliens, sb):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            check_keydown_events(event, ai_settings, screen, ship, bullets)
        elif event.type == pygame.KEYUP:
            check_keyup_events(event, ship)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            mouse_x, mouse_y = pygame.mouse.get_pos()
            check_play_button(
                stats, play_button, mouse_x, mouse_y, 
                ai_settings, screen, ship, aliens, bullets, sb)


def check_play_button(stats, play_button, mouse_x, mouse_y, 
        ai_settings, screen, ship, aliens, bullets, sb):
    button_clicked = play_button.rect.collidepoint(mouse_x, mouse_y)
    if button_clicked and not stats.game_active:
        # сброс игровых настроек
        ai_settings.initialize_dynamic_settings()

        stats.reset_stats()
        stats.game_active = True

        # очистка пришельцев и пуль
        aliens.empty()
        bullets.empty()

        create_fleet(ai_settings, screen, ship, aliens)
        ship.center_ship()

        # скрыть курсор
        pygame.mouse.set_visible(False)

        # обновление доски счёта
        sb.refresh()


def update_screen(ai_settings, screen, ship, bullets, aliens, stats, play_button, sb):
    # перерисовка экрана
    screen.fill(ai_settings.bg_color)

    # все пули выводятся позади корабля и пришельцев
    for bullet in bullets.sprites():
        bullet.draw_bullet()
        
    ship.blitme()    
    aliens.draw(screen)

    # вывод счёта
    sb.show_score()

    # не отображать Play если кнопка активна
    if not stats.game_active:
        play_button.draw_button()

    # отоборажение экрана
    pygame.display.flip()


def update_bullets(bullets, aliens, ai_settings, screen, ship, stats, sb):
    # обновление позиции пуль
    bullets.update()

    # Удаление пуль за экраном
    for bullet in bullets.copy():
        if bullet.rect.bottom <= 0:
            bullets.remove(bullet)
    check_bullet_alien_collisions(ai_settings, screen, ship, aliens, bullets, stats, sb)


def check_bullet_alien_collisions(ai_settings, screen, ship, aliens, bullets, stats, sb):
    # Проверка попадания
    collisions = pygame.sprite.groupcollide(bullets, aliens, ai_settings.bullet_collision_delete, True)

    if collisions:
        for aliens in collisions.values():
            stats.score += ai_settings.alien_points * len(aliens)
            sb.prep_score()
            check_high_score(stats, sb)

    if len(aliens) == 0:
        bullets.empty()
        ai_settings.increase_speed()

        # увеличение уровня
        stats.level += 1
        sb.prep_level()

        create_fleet(ai_settings=ai_settings, screen=screen, ship=ship, aliens=aliens)


def fire_bullet(ai_settings, screen, ship, bullets):
    if len(bullets) < ai_settings.bullets_allowed:
        new_bullet = Bullet(ai_settings, screen, ship)
        bullets.add(new_bullet)


def get_number_aliens_x(ai_settings, alien_width):
    # вычисляет кол-во пришельцев в ряду
    avialable_space_x = ai_settings.screen_width - 2 * alien_width
    number_aliens_x = int(avialable_space_x / (2 * alien_width))
    return number_aliens_x


def get_number_rows(ai_settings, ship_height, alien_height):
    # количество рядов инопланетян
    aviable_space_y = (
        ai_settings.screen_height - (6 * alien_height) - ship_height)
    number_rows = int(aviable_space_y / (2 * alien_height))
    return number_rows


def create_alien(ai_settings, screen, aliens: list, alien_number: int, row_number: int):
    # Создает пришельца и размещает его в ряду
    alien = Alien(ai_settings=ai_settings, screen=screen)
    alien_width = alien.rect.width
    alien.x = alien_width + 2 * alien_width * alien_number
    alien.rect.x = alien.x
    alien.rect.y = alien.rect.height + 2 * alien.rect.height * row_number
    aliens.add(alien)


def create_fleet(ai_settings, screen, ship, aliens):
    # создание флота пришельцев
    alien = Alien(ai_settings, screen)
    number_aliens_x = get_number_aliens_x(ai_settings, alien.rect.width)
    number_rows = get_number_rows(ai_settings, 
        ship.rect.height, alien.rect.height)

    for row_number in range(number_rows):
        for alien_number in range(number_aliens_x):
            create_alien(ai_settings, screen, aliens, alien_number, row_number)


def ship_hit(ai_settings, stats, screen, ship, aliens, bullets, sb):
    # уменьшение жизней
    if stats.ships_left > 0:
        stats.ships_left -= 1
        sb.prep_ships()

        # очистка пришельцев и пуль
        aliens.empty()
        bullets.empty()

        # создание нового флота и размещение корабля в центре
        create_fleet(ai_settings=ai_settings, screen=screen, 
            ship=ship, aliens=aliens)
        ship.center_ship()

        # небольшая задержка
        sleep(0.5)
    else:
        stats.game_active = False
        pygame.mouse.set_visible(True)


def check_aliens_bottom(ai_settings, stats, screen, ship, aliens, bullets, sb):
    # проверка добрались пришельцы до низа экрана
    screen_rect = screen.get_rect()
    for alien in aliens.sprites():
        if alien.rect.bottom >= screen_rect.bottom:
            ship_hit(ai_settings, stats, screen, ship, aliens, bullets, sb)
            break


def update_aliens(ai_settings, aliens, ship, stats, screen, bullets, sb):
    check_fleet_edges(ai_settings, aliens)
    aliens.update()

    # проверка столкновения корабля с пришельцем
    if pygame.sprite.spritecollideany(ship, aliens):
        ship_hit(ai_settings, stats, screen, ship, aliens, bullets, sb)

    # проверка столкновения с низом экрана    
    check_aliens_bottom(ai_settings, stats, screen, ship, aliens, bullets, sb)


def check_fleet_edges(ai_settings, aliens):
    # проверяет достижение края экрана пришельцем
    for alien in aliens.sprites():
        if alien.check_edges():
            change_fleet_direction(ai_settings, aliens)
            break


def change_fleet_direction(ai_settings, aliens):
    # отпускает весь флот и меняет направление
    for alien in aliens.sprites():
        alien.rect.y += ai_settings.fleet_drop_speed
    ai_settings.fleet_direction *= -1


def check_high_score(stats, sb):
    # проверка, появился ли новый рекорд
    if stats.score > stats.high_score:
        stats.high_score = stats.score
        sb.prep_high_score()
