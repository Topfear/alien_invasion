## Игра Alien Invasion

Игра в стиле ретро-шутер. Нужно уничтожить пришельцев, пока они не достигли нижнего края экрана или корабля игрока.     
Управление:
- ⬆️, ⬅️, ⬇️, ➡️ - перемещение корабля
- Space - выстрел
- Q - выйти из игры


---

## Установка

Для установки необходимо:
1. Скачать python [https://www.python.org/downloads/](https://www.python.org/downloads/ "Скачать python")
2. Скачать проект ```git clone https://gitlab.com/Topfear/alien_invasion.git```
3. Запустить файл ```build.bat```
4. Запустить файл ```start.bat```
